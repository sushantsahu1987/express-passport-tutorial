var express = require('express');
var app = express();
var router = express.Router();
var session = require('express-session');
var MySQLStore = require('express-mysql-session')(session);
var cookieParser = require('cookie-parser');
var passport = require('passport');
var Strategy = require('passport-local').Strategy;
var db = require('./db');
var bodyparser = require('body-parser');


passport.use( new Strategy(

  function(username, password, cb) {

    db.users.findByUsername(username, function(err,user) {

        console.log('passport : strategy '+username);
        console.log('passport : strategy user '+user.password);
        console.log('passport : strategy user '+user.id);

        if(err){ return cb(err);}
        if(!user){return cb(null, false);}
        if(user.password != password) {return cb(null,false);}
        return cb(null,user);
    });

  }

));

passport.serializeUser(

  function(user,cb) {
    console.log('passport : serializeUser');
    cb(null,user.id);
  }
);

passport.deserializeUser(function(id,cb){

  console.log('passport : deserializeUser');

  db.users.findById(id, function(err,user) {

    if(err) {return cb(err);}
    console.log('passport : deserializeUser user '+user);
    cb(null,user);
  });
});



var options = {
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: 'VonNeumann1987$',
  database: 'session_test',
  checkExpirationInterval: 60000,
  expiration: 45000,
  createDatabaseTable: true,
  schema: {
    tablename: 'session',
    columnNames: {
        session_id : 'session_id',
        expires: 'expires',
        data: 'data'

    }
  }
};

var sessionStore = new MySQLStore(options);
app.use(bodyparser.json());
app.use(cookieParser());
app.use(function(req,resp,next) {
    console.log('req '+req.path);
    next();
});

app.use(session({
  key:'session_name',
  name:'my_passport_local_app',
  secret:'random_gen_session_key_secret',
  store: sessionStore,
  resave: true,
  saveUnitialized: false,
  cookie: { maxAge : 45000 }
}));

app.use(passport.initialize());
app.use(passport.session());


router.post('/login',  passport.authenticate('local',{
    successRedirect: '/loginSuccess',
    failureRedirect: '/loginFailure'
  })

);

router.post('/auth', function(req, res){
  console.log("body parsing", req.body);
  res.send('/auth request');

});

router.post('/logout',function(req,resp) {
  console.log('post: logout ');
});

router.get('/profile', require('connect-ensure-login').ensureLoggedIn('/loginFailure'),
  function(req,resp) {
    console.log('get req session '+JSON.stringify(req.session));
    console.log('get: profile '+req.session.passport.user);
    resp.json({msg:'profile'});
});

router.get('/loginSuccess',function(req,resp){
  console.log('get: loginSuccess ');
  resp.json({msg:'success'});
});

router.get('/loginFailure',function(req,resp) {
  console.log('get: loginFailure ');
  resp.json({msg:'failure'});
});

router.get('/users', function(req, resp) {
  resp.send('request users');
});

app.use(router);

app.listen(3000, function() {
  console.log('listening at port 3000 ');
});
